﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TaxiBooking.Context;
using TaxiBooking.ViewModels;

namespace TaxiBooking.Services.CarService
{
    public class CarService : ICarService
    {
        private readonly ApplicationDbContext _context;
        public CarService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<bool> AddCar(CreateCarModel model)
        {
            try
            {
                TaxiBooking.Models.Car car = new TaxiBooking.Models.Car()
                {
                    CarNo = model.CarNo,
                    CarColor = model.CarColor,
                    CarModel = model.CarModel,
                    ExpiryDate = model.ExpiryDate,
                    CreatedAt = model.CreatedAt,
                    OwnerName = model.OwnerName,
                };
                _context.Cars.Add(car);
                _context.SaveChanges();

                List<TaxiBooking.Models.Attachment> list = new List<TaxiBooking.Models.Attachment>();
                foreach (var item in model.Attachments)
                {
                    TaxiBooking.Models.Attachment attachment = new TaxiBooking.Models.Attachment()
                    {
                        CarId = car.Id,
                        File = "",
                        Type = 1
                    };
                    list.Add(attachment);
                }
                _context.Attachments.AddRange(list);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<TaxiBooking.Models.Car> GetCar(int Id)
        {
            try
            {
                return _context.Cars.Where(x => x.IsActive && x.Id == Id).Include(x => x.Attachments).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<TaxiBooking.Models.Car>> ListCar()
        {
            try
            {
                return _context.Cars.Where(x => x.IsActive).Include(x => x.Attachments).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<bool> UpdateCar(UpdateCarModel model)
        {
            try
            {
                TaxiBooking.Models.Car car = _context.Cars.Find(model.Id);
                if (car != null)
                {
                    car.CarNo = model.CarNo;
                    car.CarColor = model.CarColor;
                    car.CarModel = model.CarModel;
                    car.ExpiryDate = model.ExpiryDate;
                    car.CreatedAt = model.CreatedAt;
                    car.OwnerName = model.OwnerName;

                    _context.Entry(car).State = EntityState.Modified;
                    _context.SaveChanges();

                    if (model.Attachments.Any())
                    {
                        var attachments = await _context.Attachments.Where(a => a.Id == model.Id).ToListAsync();
                        _context.Attachments.RemoveRange(attachments);
                        List<TaxiBooking.Models.Attachment> list = new List<TaxiBooking.Models.Attachment>();
                        foreach (var item in model.Attachments)
                        {
                            TaxiBooking.Models.Attachment attachment = new TaxiBooking.Models.Attachment()
                            {
                                CarId = car.Id,
                                File = "",
                                Type = 1
                            };
                            list.Add(attachment);
                        }
                        _context.Attachments.AddRange(list);
                        _context.SaveChanges();
                    }

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}