﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiBooking.ViewModels;

namespace TaxiBooking.Services.CarService
{
    public interface ICarService
    {
        Task<bool> AddCar(CreateCarModel model);
        Task<bool> UpdateCar(UpdateCarModel model);
        Task<TaxiBooking.Models.Car> GetCar(int Id);
        Task<List<TaxiBooking.Models.Car>> ListCar();
    }
}
