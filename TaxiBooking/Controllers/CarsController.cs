﻿using TaxiBooking.Context;
using TaxiBooking.Models;
using TaxiBooking.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Collections.Generic;
using System.Net;

namespace TaxiBooking.Controllers
{
    public class CarsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CarsController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Cars
        public ActionResult Index()
        {
            IEnumerable<Car> cars = _context.Cars.Include(x => x.Attachments).ToList();
            return View(cars);
        }

        [HttpGet]
        public ActionResult Details(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var cars = _context.Cars.Find(id);
            if(cars == null)
            return HttpNotFound();

            return View(cars);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(CreateCarModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Attachments = model.Attachments;
                return View("Create", model);
            }
            Car car = new Car()
            {
                //Attachments = model.Attachments,
                CarNo = model.CarNo,
                CarColor = model.CarColor,
                CarModel = model.CarModel,
                ExpiryDate = model.ExpiryDate,
                CreatedAt = model.CreatedAt,
                OwnerName = model.OwnerName,
            };
            _context.Cars.Add(car);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}