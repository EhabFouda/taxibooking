﻿namespace TaxiBooking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create_TaxiBookingDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CarId = c.Int(nullable: false),
                        File = c.String(nullable: false),
                        Type = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cars", t => t.CarId, cascadeDelete: true)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarNo = c.Int(nullable: false),
                        CarColor = c.String(nullable: false, maxLength: 256),
                        CarModel = c.String(nullable: false, maxLength: 256),
                        ExpiryDate = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        OwnerName = c.String(nullable: false, maxLength: 256),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attachments", "CarId", "dbo.Cars");
            DropIndex("dbo.Attachments", new[] { "CarId" });
            DropTable("dbo.Cars");
            DropTable("dbo.Attachments");
        }
    }
}
