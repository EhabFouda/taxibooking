﻿using TaxiBooking.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using TaxiBooking.Services.CarService;

namespace TaxiBooking.Handler.Query.Car.List
{
    public class ListCarQueryHandler : IRequestHandler<ListCarQuery, IEnumerable<TaxiBooking.Models.Car>>
    {
        private readonly ICarService _car;

        public ListCarQueryHandler(ICarService car)
        {
            _car = car;
        }
        public async Task<IEnumerable<Models.Car>> Handle(ListCarQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return await _car.ListCar();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}