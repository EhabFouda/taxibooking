﻿using MediatR;
using System.Collections.Generic;

namespace TaxiBooking.Handler.Query.Car.List
{
    public class ListCarQuery : IRequest<IEnumerable<TaxiBooking.Models.Car>>
    {
    }
}