﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaxiBooking.Handler.Query.Car.Details
{
    public class GetCarQuery : IRequest<TaxiBooking.Models.Car>
    {
        [Required]
        public int Id { get; set; }
    }
}