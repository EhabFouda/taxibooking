﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TaxiBooking.Context;
using TaxiBooking.Services.CarService;

namespace TaxiBooking.Handler.Query.Car.Details
{
    public class GetCarQueryHandler : IRequestHandler<GetCarQuery, TaxiBooking.Models.Car>
    {
        private readonly ICarService _car;

        public GetCarQueryHandler(ICarService car)
        {
            _car = car;
        }
        public async Task<Models.Car> Handle(GetCarQuery request, CancellationToken cancellationToken)
        {
           return await _car.GetCar(request.Id);
        }
    }
}