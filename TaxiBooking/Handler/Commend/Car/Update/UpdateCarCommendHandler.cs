﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TaxiBooking.Context;
using TaxiBooking.Services.CarService;
using TaxiBooking.ViewModels;

namespace TaxiBooking.Handler.Commend.Car.Update
{
    public class UpdateCarCommendHandler : IRequestHandler<UpdateCarCommend, bool>
    {
        private readonly ICarService _car;

        public UpdateCarCommendHandler(ICarService car)
        {
            _car = car;
        }
        public async Task<bool> Handle(UpdateCarCommend request, CancellationToken cancellationToken)
        {
            try
            {
                UpdateCarModel car = new UpdateCarModel()
                {
                    Id = request.Id,
                    CarNo = request.CarNo,
                    CarColor = request.CarColor,
                    CarModel = request.CarModel,
                    ExpiryDate = request.ExpiryDate,
                    CreatedAt = request.CreatedAt,
                    OwnerName = request.OwnerName,
                    Attachments = request.Attachments,
                };
                return await _car.UpdateCar(car);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}