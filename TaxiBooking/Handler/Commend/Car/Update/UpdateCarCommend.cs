﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaxiBooking.Handler.Commend.Car.Update
{
    public class UpdateCarCommend : IRequest<bool>
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int CarNo { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarColor { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarModel { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        [MaxLength(256)]
        public string OwnerName { get; set; }
        public List<IFormFile> Attachments { get; set; }
    }
}