﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaxiBooking.Handler.Commend.Car.Add
{
    public class AddCarCommend : IRequest<bool>
    {
        [Required]
        public int CarNo { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarColor { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarModel { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        [MaxLength(256)]
        public string OwnerName { get; set; }
        public List<IFormFile> Attachments { get; set; }
    }
}