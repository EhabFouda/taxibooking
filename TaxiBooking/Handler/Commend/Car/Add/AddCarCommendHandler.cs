﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TaxiBooking.Context;
using TaxiBooking.Services.CarService;
using TaxiBooking.ViewModels;

namespace TaxiBooking.Handler.Commend.Car.Add
{
    public class AddCarCommendHandler : IRequestHandler<AddCarCommend, bool>
    {
        private readonly ICarService _car;

        public AddCarCommendHandler(ICarService car)
        {
            _car = car;
        }
        public async Task<bool> Handle(AddCarCommend request, CancellationToken cancellationToken)
        {
            try
            {
                CreateCarModel car = new CreateCarModel()
                {
                    CarNo = request.CarNo,
                    CarColor = request.CarColor,
                    CarModel = request.CarModel,
                    ExpiryDate = request.ExpiryDate,
                    CreatedAt = request.CreatedAt,
                    OwnerName = request.OwnerName,
                    Attachments = request.Attachments,
                };
                return await _car.AddCar(car);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}