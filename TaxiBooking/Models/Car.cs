﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiBooking.Models
{
    public class Car
    {
        public Car()
        {
            Attachments = new HashSet<Attachment>();
            IsActive = true;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int CarNo { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarColor { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarModel { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        [MaxLength(256)]
        public string OwnerName { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
    }
}