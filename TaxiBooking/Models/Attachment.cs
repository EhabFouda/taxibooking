﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TaxiBooking.Models
{
    public class Attachment
    {
        public Attachment()
        {
            CreatedAt = DateTime.Now;
            IsActive = true;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public int CarId { get; set; }
        [Required]
        public string File { get; set; }
        [Required]
        public int Type { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public virtual Car Car { get; set; }
    }
}