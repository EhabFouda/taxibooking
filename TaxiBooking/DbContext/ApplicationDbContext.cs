﻿using TaxiBooking.Models;
using System.Data.Entity;

namespace TaxiBooking.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
        }

        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Car> Cars { get; set; }
    }
}