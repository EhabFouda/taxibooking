﻿using TaxiBooking.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using Microsoft.AspNetCore.Http;

namespace TaxiBooking.ViewModels
{
    public class CreateCarModel
    {
        [Required]
        public int CarNo { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarColor { get; set; }
        [Required]
        [MaxLength(256)]
        public string CarModel { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        [MaxLength(256)]
        public string OwnerName { get; set; }
        public List<IFormFile> Attachments { get; set; }
    }
}