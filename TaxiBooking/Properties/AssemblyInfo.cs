﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TaxiBooking")]
[assembly: AssemblyDescription("TaxiBooking")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TaxiBooking")]
[assembly: AssemblyProduct("TaxiBooking")]
[assembly: AssemblyCopyright("Copyright TaxiBooking ©  2024")]
[assembly: AssemblyTrademark("TaxiBooking")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e8117b3c-9b65-4089-bbfd-40153a688e7a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
